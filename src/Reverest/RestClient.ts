import { RestCollectorClient, DecorateRequest, RestCollectorRequest }  from "rest-collector";
import { Request } from "express";

export class RestClient<E=any> extends RestCollectorClient<E,Request>{
    constructor(entityRestAPI: string, decorateRequests?: DecorateRequest<Request>) {
        super(entityRestAPI, {
            decorateRequest: (req: RestCollectorRequest, bag?: Request): void => {
                if(bag) {
                    if(bag.headers) {
                        req.headers.tid = bag.headers.tid;
                    }
                    const user: any = (bag as any).user;
                    if(user) {
                        req.headers.userid = user.id

                    }
                }

                if(decorateRequests) {
                    decorateRequests.decorateRequest(req, bag);
                }
            }
        })
    }

}