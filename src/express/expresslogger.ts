import * as express from 'express';
import { Request, Response } from 'express'
import * as winston from "winston";
import * as expressWinston from "express-winston";

let logger: winston.LoggerInstance;
const transports: Array<any> = [];

export function bindLogger (app: express.Express, MODE: string) {

    transports.push(new(winston.transports.Console)());
    if (transports.length > 0 ){
        app.use(expressWinston.logger({
            transports: transports,
            msg: "{{req.header.tid}} HTTP {{req.method}} {{res.statusCode}} {{req.url}} {{res.responseTime}} {{req.headers.userid }} {{JSON.stringify(req.lastError)}} {{req.lastError ? req.lastError.stack: '' }}",
            expressFormat: false, // Use the default Express/morgan request formatting, with the same colors. Enabling this will override any msg and colorStatus if true. Will only output colors on transports with colorize set to true
            ignoreRoute: function(req: Request, res: Response) { return false; } // optional: allows to skip some log messages based on request and/or response
        }));
        logger = new winston.Logger({
            transports:  transports
        });
    }
}

export class Logger {
    public static debug(message: string) : void{
        logger.level= "debug";
        logger.debug(process.env.SERVICE_NAME + "\r\n" + message);
    }

    public static error(message: string) : void{
        logger.level= "error";
        logger.debug(process.env.SERVICE_NAME + "\r\n" + message);
    }

    public static info(message: string) : void{
        logger.level= "info";
        logger.debug(process.env.SERVICE_NAME + "\r\n"+ message);
    }
}