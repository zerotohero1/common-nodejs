import express, { Express, Request, Response, NextFunction } from 'express';
import cookieParser from 'cookie-parser';
import bodyParser from 'body-parser';
import { bindLogger, Logger } from './expresslogger';
import TarRequest from '../interfaces/TarRequest';
import * as uuid from 'uuid';

export function createExpressInstance(serviceName: string, MODE: string): Express {
    process.env.SERVICE_NAME = serviceName;

    const app = express();

    app.use( bodyParser.json({limit: '50mb'}) );
    app.use(cookieParser());
    app.use(bodyParser.urlencoded({
        limit: '50mb',
        extended: true,
        parameterLimit:50000
    }));

    app.disable('etag');
    app.disable('view cache');

    app.use((req: Request, res: Response, next: NextFunction) => {
        req.headers.tid = req.headers.tid || uuid.v4();
        next();
    });
    app.get('/', (req: Request, res: Response, next: NextFunction) => {
        res.status(200).send();
    });

    app.get('/status', (req: Request, res: Response, next: NextFunction) => {
        res.status(200).send("OK");
    });

    bindLogger(app, MODE);
    return app;
}

export function initializeErrorHandler(app: Express) {
    app.use((err: any, req: TarRequest, res: Response, next: NextFunction) => {
        req.lastError = err;
        res.status(err.status || 500).send(err); 
    });
}

export { Logger };