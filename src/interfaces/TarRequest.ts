import { Request } from 'express'

export default interface TarRequest extends Request {
    lastError?: any
}